package com.wangyubing.netty.mychat;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author wangyubing
 * @emil 1326124948@qq.com
 */
public class MyChatClient {

	public static void main(String[] args) throws InterruptedException, IOException {
		// 创建
		EventLoopGroup boseGroup = new NioEventLoopGroup();

		// 创建启动器
		Bootstrap bootstrap = new Bootstrap();
		bootstrap.group(boseGroup).channel(NioSocketChannel.class).
				handler(new MyChatClienInitilalizer());

		// 启动
		ChannelFuture future = bootstrap.connect("localhost", 8899).sync();
		Channel channel = future.channel();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (br.readLine() != null) {
			channel.writeAndFlush(br.readLine());
		}
		boseGroup.shutdownGracefully();
	}
}
