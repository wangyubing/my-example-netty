package com.wangyubing.netty.mychat;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.net.SocketAddress;

/**
 * 处理器
 *
 * @author wangyubing
 * @emil 1326124948@qq.com
 */
public class MyChatServerHanlder extends ChannelInboundHandlerAdapter {
	/**
	 * 需要一个组
	 */
	private static volatile ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

		Channel channel = ctx.channel();
		SocketAddress socketAddress = channel.remoteAddress();
		/// 处理程序
		channels.forEach(ch -> {
			if (ch != channel) {
				ch.writeAndFlush("[" + socketAddress + "] " + msg + " \n");
			} else {
				ch.writeAndFlush("[自己] " + msg + " \n");
			}
		});

	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		System.out.println(channels.size());
		SocketAddress socketAddress = ctx.channel().remoteAddress();
		channels.add(ctx.channel());
		channels.writeAndFlush("[服务端] " + socketAddress + " 上线\n");
	}


	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		SocketAddress socketAddress = ctx.channel().remoteAddress();
		channels.remove(ctx.channel());
		channels.writeAndFlush("[服务端] " + socketAddress + " 下线\n");
	}

	@Override
	public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();
		SocketAddress socketAddress = channel.remoteAddress();
		channels.add(channel);
		channels.writeAndFlush("[服务端] " + socketAddress + " 加入聊天室\n");

	}


	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();
		SocketAddress socketAddress = channel.remoteAddress();
		channels.remove(channel);
		channels.writeAndFlush("[服务端] " + socketAddress + " 退出聊天室\n");
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		if (evt instanceof IdleStateEvent) {
			// 超时事件
			IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
			String idleType = null;
			switch (idleStateEvent.state()) {
				case ALL_IDLE:
					idleType = "读写超时";
					break;
				case READER_IDLE:
					idleType = "读超时";
					break;
				case WRITER_IDLE:
					idleType = "写超时";
					break;
				default:

			}
			System.out.println("超时事件 " + idleType);
			channels.remove(ctx.channel());
			ctx.channel().close();
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.channel().close();
		channels.remove(ctx.channel());
	}
}


