package com.wangyubing.netty.mychat;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author wangyubing
 * @emil 1326124948@qq.com
 */
public class MyChatServer {
	public static void main(String[] args) throws InterruptedException {
		// 创建两个
		EventLoopGroup boseGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();

		// 创建启动器
		ServerBootstrap serverBootstrap = new ServerBootstrap();
		serverBootstrap.
				group(boseGroup,workerGroup).
				channel(NioServerSocketChannel.class).
				handler(new LoggingHandler(LogLevel.INFO)).
				childHandler(new MyChatServerInitilalizer());

		// 启动
		ChannelFuture future = serverBootstrap.bind("localhost", 8899).sync();
		future.channel().closeFuture().sync();

		workerGroup.shutdownGracefully();
		boseGroup.shutdownGracefully();
	}
}
