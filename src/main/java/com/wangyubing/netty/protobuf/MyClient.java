package com.wangyubing.netty.protobuf;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author wangyubing
 * @emil 1326124948@qq.com
 */
public class MyClient {

	public static void main(String[] args) {
		EventLoopGroup boss = new NioEventLoopGroup();
		try {
			Bootstrap bootstrap = new Bootstrap();
			bootstrap.
					group(boss).
					channel(NioSocketChannel.class).
					handler(new MyClientInitializer());

			ChannelFuture sync = bootstrap.connect("localhost", 8899).sync();
			sync.channel().closeFuture().sync();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			boss.shutdownGracefully();
		}

	}
}
