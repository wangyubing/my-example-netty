package com.wangyubing.netty.protobuf;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author wangyubing
 * @emil 1326124948@qq.com
 */
public class MyClientHandler extends SimpleChannelInboundHandler<MyDataInfo.Person> {

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, MyDataInfo.Person msg) throws Exception {
		System.out.println(msg.toString());
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		MyDataInfo.Person build = MyDataInfo.Person.newBuilder().setName("哈哈").setAge(20).setAddress("北京").build();
		ctx.writeAndFlush(build);
	}
}
