package com.wangyubing.netty.protobuf;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author wangyubing
 * @emil 1326124948@qq.com
 */
public class MyServerHandler extends SimpleChannelInboundHandler<MyDataInfo.Person> {

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, MyDataInfo.Person msg) throws Exception {
		System.out.println(msg.getName() + "---" + msg.getAddress() + "---" + msg.getAge());
		MyDataInfo.Person build = MyDataInfo.Person.newBuilder().setAddress("北京").setAge(20).setName("我").build();
		ctx.channel().writeAndFlush(build);
	}
}
