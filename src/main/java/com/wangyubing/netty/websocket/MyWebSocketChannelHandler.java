package com.wangyubing.netty.websocket;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import java.time.LocalDateTime;

/**
 * @author wangyubing
 * @emil 1326124948@qq.com
 */
public class MyWebSocketChannelHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) {
		System.out.println(msg.text());
		ctx.channel().writeAndFlush(new TextWebSocketFrame("hello world"+ LocalDateTime.now()));
	}

	@Override
	public void handlerAdded(ChannelHandlerContext ctx) {
		System.out.println(ctx.channel().id() + "已连接");
	}

	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		System.out.println(ctx.channel().id() + "断开连接");
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}
}
