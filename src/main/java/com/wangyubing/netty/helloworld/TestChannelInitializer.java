package com.wangyubing.netty.helloworld;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.HttpServerCodec;

/**
 * @author wangyubing
 * @emil 1326124948@qq.com
 */
public class TestChannelInitializer extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		// server端发送的是httpResponse，所以要使用HttpResponseEncoder进行编码
		ch.pipeline().addLast(
				new HttpResponseEncoder());
		// server端接收到的是httpRequest，所以要使用HttpRequestDecoder进行解码
		ch.pipeline().addLast(
				new HttpRequestDecoder());
		ch.pipeline().addLast(
				new TestChannelHandler());
		//增加自定义实现的Handler
		ch.pipeline().addLast(new HttpServerCodec());
	}
}
