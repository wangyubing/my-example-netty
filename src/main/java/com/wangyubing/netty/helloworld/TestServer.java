package com.wangyubing.netty.helloworld;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.bootstrap.ServerBootstrapConfig;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetAddress;

/**
 * @author wangyubing
 * @emil 1326124948@qq.com
 */
public class TestServer {

	public static void main(String[] args) throws InterruptedException {
		// bose and work
		EventLoopGroup boseGroup = new NioEventLoopGroup();
		EventLoopGroup workGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap serverBootstrap = new ServerBootstrap();
			serverBootstrap.group(boseGroup, workGroup).channel(NioServerSocketChannel.class).
					childHandler(new TestChannelInitializer());

			ChannelFuture sync = serverBootstrap.bind(8080).sync();
			sync.channel().closeFuture().sync();
		} finally {
			workGroup.shutdownGracefully();
			boseGroup.shutdownGracefully();
		}
	}
}
